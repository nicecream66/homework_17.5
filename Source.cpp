#include<iostream>



class VectorPublic
{
private:
	double x;
	double y;
	double z;
public:
	VectorPublic(double x, double y, double z) : x(x), y(y), z(z)
	{}

	void ShowXYZ()
	{
		std::cout << x << ' ' << y << ' ' << z;

	}

	void ShowModule()
	{
		std::cout<< '\n' << sqrt(x * x + y * y + z * z);
	}
};



int main()
{
	VectorPublic v(7, 5, 3);
	v.ShowXYZ();
	v.ShowModule();
}
